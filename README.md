# Инициализация

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt -U
```

# Добавить пустую миграцию
```bash
./manage.py makemigrations --name fill_cat_breeds some_app --empty
```

# Заполнить базу

```bash
./manage.py migrate
./manage.py create_cat_breeds
./manage.py create_cats
```

# Заполнить породы котов миграцией - сначала ревертим все миграции, потом выполняем все миграции еще раз
```bash
./manage.py migrate some_app 0001
./manage.py migrate
```
