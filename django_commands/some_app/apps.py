from django.apps import AppConfig


class SomeApp(AppConfig):
    name = 'django_commands.some_app'
    verbose_name = 'Some app'
