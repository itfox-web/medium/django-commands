from django.db import models


class CatBreeds(models.Model):
    breed_name = models.CharField(
        max_length=255, unique=True, blank=False, verbose_name='Порода кота')

    def __str__(self):
        return f'Порода {self.breed_name}'

    class Meta:
        verbose_name = 'Порода кота'
        verbose_name_plural = 'Попроды котов'


class Cats(models.Model):
    name = models.CharField(max_length=255, unique=True,
                            blank=False, verbose_name='Имя кота')
    cat_breed = models.ForeignKey(CatBreeds, null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'Кот {self.name}'

    class Meta:
        verbose_name = 'Кот'
        verbose_name_plural = 'Котов'