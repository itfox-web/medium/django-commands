from django.core.management.base import BaseCommand
from annoying.functions import get_object_or_None

from django_commands.some_app.models import CatBreeds

CAT_BREED_NAMES = ['Австралийский короткохвостый', 'Уральский норный',
                   'Монгольский дикий', 'Финский спокойный', 'Полинезийский лапчатый']


class Command(BaseCommand):
    help = 'Add default cat breeds'

    def handle(self, *args, **kwargs):
        for cat_breed_name in CAT_BREED_NAMES:
            cat_breed_object = get_object_or_None(
                CatBreeds, breed_name=cat_breed_name)

            if cat_breed_object is None:
                cat_breed_object = CatBreeds()

            cat_breed_object.breed_name = cat_breed_name

            cat_breed_object.save()

        print('All default cat breeds updated. Have a nice day!')
