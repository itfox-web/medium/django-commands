from django.core.management.base import BaseCommand
from annoying.functions import get_object_or_None

from django_commands.some_app.models import Cats

CAT_NAMES = ['Василий', 'Пётр', 'Лаваш', 'Мурлок', 'Сырок', 'Хвост']


class Command(BaseCommand):
    help = 'Add default cats'

    def handle(self, *args, **kwargs):
        for cat_name in CAT_NAMES:
            cat_object = get_object_or_None(
                Cats, name=cat_name)

            if cat_object is None:
                cat_object = Cats()

            cat_object.name = cat_name

            cat_object.save()

        print('All default cats updated. Have a nice day!')
