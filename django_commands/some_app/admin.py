from django.contrib import admin
from .models import CatBreeds, Cats

@admin.register(Cats)
class CatsAdmin(admin.ModelAdmin):
    pass

@admin.register(CatBreeds)
class CatBreedsAdmin(admin.ModelAdmin):
    pass
